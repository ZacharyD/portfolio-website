import React from 'react'

const Marquee = (props) => {
    return (
        <div className="digital-marque-sec">
            <div className="container">
                <div className="digital-marque">
                    <div className="marquee">
                        <div className="track">
                            <div className="content">
                                <h1><span>Engineering Your Dreams into Reality </span> <i>-</i> <span>
                                    Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today!</span>
                                    <i>-</i> <span>Engineering Your Dreams into Reality</span> <i>-</i>
                                    <span>Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today!</span>
                                    <span>Engineering Your Dreams into Reality </span> <i>-</i> <span>
                                    Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today! </span>
                                    <i>-</i> <span>Engineering Your Dreams into Reality</span> <i>-</i>
                                    <span>Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today! </span>
                                    <span>Engineering Your Dreams into Reality </span> <i>-</i> <span>
                                    Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today! </span>
                                    <i>-</i> <span>Engineering Your Dreams into Reality</span> <i>-</i>
                                    <span>Let's Build Something Amazing</span><i>-</i> <span>Contact Me Today! </span>
                                    <span>Engineering Your Dreams into Reality </span> <i>-</i> <span>
                                    Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today! </span>
                                    <i>-</i> <span>Engineering Your Dreams into Reality</span> <i>-</i>
                                    <span>Let's Build Something Amazing </span><i>-</i> <span>Contact Me Today! </span>
                                    <span>Engineering Your Dreams into Reality </span> <i>-</i> <span>
                                    Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today! </span>
                                    <i>-</i> <span>Engineering Your Dreams into Reality</span> <i>-</i>
                                    <span>Let's Build Something Amazing</span><i>-</i> <span>Contact Me Today! </span>
                                    <span>Engineering Your Dreams into Reality </span> <i>-</i> <span>
                                    Let's Build Something Amazing</span> <i>-</i> <span>Contact Me Today! </span>
                                    <i>-</i> <span>Engineering Your Dreams into Reality</span> <i>-</i>
                                    <span>Let's Build Something Amazing</span><i>-</i> <span>Contact Me Today! </span>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Marquee;
