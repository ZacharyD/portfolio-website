// import React from 'react'
// import Link from 'next/link'
// import Logo from '/public/images/ZD.png'
// import Image from 'next/image'


// const Footer = (props) => {
//     return (
//         <div className="tp-site-footer text-center">
//             <div className="container">
//                 <div className="row">
//                     <div className="col-12">
//                         <div className="footer-image">
//                             <Link className="logo" href="/"><Image src={Logo} alt="" /></Link>
//                         </div>
//                     </div>
//                     <div className="col-12">
//                         <div className="link-widget">
//                             <ul>
//                                 <li><Link href="https://www.youtube.com/@ZachDempsey/featured"><i className="ti-youtube"></i></Link></li>
//                                 <li><Link href="https://www.linkedin.com/in/zdempsey/"><i className="ti-linkedin"></i></Link></li>
//                                 <li><Link href="https://github.com/ZDempsey1"><i className="ti-github"></i></Link></li>
//                                 <li><Link href="https://gitlab.com/ZacharyD"></Link></li>
//                             </ul>
//                         </div>
//                     </div>
//                     <div className="col-12">
//                         <div className="copyright">
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     )
// }

// export default Footer;
import React from 'react'
import Link from 'next/link'
import Logo from '/public/images/ZD.png'
import Image from 'next/image'

const Footer = (props) => {
    return (
        <div className="tp-site-footer text-center">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="footer-image">
                            <Link className="logo" href="/"><Image src={Logo} alt="" /></Link>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="link-widget">
                            <ul>
                                <li><Link href="https://www.youtube.com/@ZachDempsey/featured"><i className="ti-youtube"></i></Link></li>
                                <li><Link href="https://www.linkedin.com/in/zdempsey/"><i className="ti-linkedin"></i></Link></li>
                                <li><Link href="https://github.com/ZDempsey1"><i className="ti-github"></i></Link></li>
                                <li><Link href="https://gitlab.com/ZacharyD"><i style={{ backgroundImage: "url('https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png')", backgroundPosition: "center", backgroundRepeat: "no-repeat", backgroundSize: "contain", height: "50%", width: "50%", filter: "invert(1)" }}></i></Link></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="copyright">
                        </div>
                    </div>
                </div>
            </div>
            <style jsx>{`
              .gitlab-link {
                background-image: url("https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png");
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
                height: 50%;
                width: 50%;
              }
            `}</style>
        </div>
    )
}

export default Footer;
