
import React, { Fragment } from 'react';
import { Dialog, Grid, } from '@mui/material'
import Contact from './contact';
import RelatedProject from './related';
import Image from 'next/image';


const ProjectSingle = ({ maxWidth, open, onClose, title, pImg, psub1img1, psub1img2, about_1, about_2, location, location_value, client, client_value, consult, consult_value, project_type, project_type_value, duration, duration_value, completion, completion_value, strategies, strat_1, strat_2, strat_3, strat_4, strat_5, approach, goals_1, goals_2, goals_3, goals_4, results_1, results_2, results_3, results_4, quote, name, relation,}) => {


    return (
        <Fragment>
            <Dialog
                open={open}
                onClose={onClose}
                className="modalWrapper quickview-dialog"
                maxWidth={maxWidth}
            >

                <Grid className="modalBody modal-body project-modal">
                    <button className='modal-close' onClick={onClose}><i className='fa fa-close'></i></button>
                    <div className="tp-project-single-area">
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-lg-12 col-12">
                                    <div className="tp-project-single-wrap">
                                        <div className="tp-project-single-item">
                                            <div className="row align-items-center mb-5">
                                                <div className="col-lg-7">
                                                    <div className="tp-project-single-title">
                                                        <h3>{title} Project</h3>
                                                    </div>
                                                    <p>{about_1}</p>
                                                    <p>{about_2}</p>
                                                </div>
                                                <div className="col-lg-5">
                                                    <div className="tp-project-single-content-des-right">
                                                        <ul>
                                                            <li>{location}<span>{location_value}</span></li>
                                                            <li>{client}<span>{client_value}</span></li>
                                                            <li>{consult}<span>{consult_value}</span></li>
                                                            <li>{project_type}<span>{project_type_value} </span></li>
                                                            <li>{duration}<span>{duration_value}</span></li>
                                                            <li>{completion}<span>{completion_value}</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tp-project-single-main-img">
                                                <Image src={pImg} alt="" />
                                            </div>
                                        </div>
                                        <div className="tp-project-single-item list-widget">
                                            <div className="row">
                                                <div className="col-lg-6">
                                                    <div className="tp-project-single-title">
                                                        <h3>Our Strategies</h3>
                                                    </div>
                                                    <p>{strategies}</p>
                                                    <ul>
                                                        <li>{strat_1}</li>
                                                        <li>{strat_2}</li>
                                                        <li>{strat_3}</li>
                                                        <li>{strat_4}</li>
                                                        <li>{strat_5}</li>
                                                    </ul>
                                                </div>
                                                <div className="col-lg-6">
                                                    <div className="tp-project-single-item-quote">
                                                        <p>{quote}</p>
                                                        <span>{name} <span>{relation}</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="tp-project-single-item">
                                            <div className="tp-project-single-title">
                                                <h3>Our approach</h3>
                                            </div>
                                            <p>{approach}</p>
                                        </div>
                                        <div className="tp-project-single-gallery">
                                            <div className="row mt-4">
                                                <div className="col-md-6 col-sm-6 col-12">
                                                    <div className="tp-p-details-img">
                                                        <Image src={psub1img1} alt="" />
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 col-12">
                                                    <div className="tp-p-details-img">
                                                        <Image src={psub1img2} alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="tp-project-single-item list-widget">
                                        {goals_1 || goals_2 || goals_3 || goals_4 || results_1 || results_2 || results_3 || results_4 ? (
                                            <div className="row">
                                            <div className="col-lg-6">
                                                <div className="tp-project-single-title">
                                                <h3>Future goals</h3>
                                                </div>
                                                <ul>
                                                {goals_1 && <li>{goals_1}</li>}
                                                {goals_2 && <li>{goals_2}</li>}
                                                {goals_3 && <li>{goals_3}</li>}
                                                {goals_4 && <li>{goals_4}</li>}
                                                </ul>
                                            </div>
                                            <div className="col-lg-6 list-widget-s">
                                                <div className="tp-project-single-title">
                                                <h3>Results</h3>
                                                </div>
                                                <ul>
                                                {results_1 && <li>{results_1}</li>}
                                                {results_2 && <li>{results_2}</li>}
                                                {results_3 && <li>{results_3}</li>}
                                                {results_4 && <li>{results_4}</li>}
                                                </ul>
                                            </div>
                                            </div>
                                        ) : null}
                                        </div>

                                        <RelatedProject />
                                        <div className="tp-project-single-item">
                                            <div className="tp-project-contact-area">
                                                <div className="tp-contact-title">
                                                    <h2>Have a project in mind? Let's discuss</h2>
                                                    <p>Get in touch with me today, to see what I can do for you!</p>
                                                </div>
                                                <div className="tp-contact-form-area">
                                                    <Contact />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Grid>
            </Dialog>
        </Fragment>
    );
}
export default ProjectSingle;
