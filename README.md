This is a starter template for [Learn Next.js](https://nextjs.org/learn).

## INSTALLATION
## Tonu is based on create-next-app and has its README.md which you can find very useful.

## To get started follow this steps:

## Install Node and npm.
## Download and open project.
## Install packages: npm i, npm install or yarn install.
## Start project locally: npm run dev or yarn start (running on port 3000).
## Make necessary changes.
## Build app for production: npm run build or yarn build.


## Configuration
## To get started follow Configuration steps:

## Install Node and npm.
## Download and open project.
## Install packages: npm i, npm install or yarn install.
## Start project locally: npm start or yarn start (running on port 3000).
## webpack.config.js
