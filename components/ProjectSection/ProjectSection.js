import Image from 'next/image';
import React, { useState } from 'react';
import Projects from '../../api/project'
import ProjectSingle from '../ProjectSingle/ProjectSingle';


const ProjectSection = (props) => {

    const [open, setOpen] = React.useState(false);

    function handleClose() {
        setOpen(false);
    }

    const [state, setState] = useState({
    })

    const [number, setCount] = useState(3);
    const [buttonActive, setButtonState] = useState(false);

    const handleClickOpen = (item) => {
        setOpen(true);
        setState(item)
    }
    return (

        <section className="tp-project-section section-padding">
            <div className="container">
                <div className="tp-section-title">
                    <span>Projects</span>
                    <h2>My Latest Projects</h2>
                </div>
                <div className="tp-project-wrap">
                    <div className="row">
                        {Projects.slice(0, number).map((project, pro) => (
                            <div className="col col-xl-4 col-lg-6 col-sm-12 col-12" key={pro}>
                                <div className="tp-project-item">
                                    <div className="tp-project-img" onClick={() => handleClickOpen(project)}>
                                        <Image src={project.pImg} alt="" />
                                    </div>
                                    <div className="tp-project-content">
                                        <span>{project.subTitle}</span>
                                        <h2 onClick={() => handleClickOpen(project)}>{project.title}</h2>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                    <div className={`project-btn ${buttonActive ? "d-none" : ""}`}>
                        <span onClick={() => setButtonState(!buttonActive)}>
                            <button className="theme-btn" onClick={() => setCount(number + number)}>View all work</button>
                        </span>
                    </div>
                </div>
            </div>
            <ProjectSingle open={open} onClose={handleClose} title={state.title} pImg={state.ps1img} psub1img1={state.psub1img1} psub1img2={state.psub1img2} about_1={state.about_1} about_2={state.about_2} location={state.location} location_value={state.location_value} client={state.client} client_value={state.client_value} consult={state.consult} consult_value={state.consult_value} project_type={state.project_type} project_type_value={state.project_type_value} duration={state.duration} duration_value={state.duration_value} completion={state.completion} completion_value={state.completion_value} strategies={state.strategies} strat_1={state.strat_1} strat_2={state.strat_2} strat_3={state.strat_3} strat_4={state.strat_4} strat_5={state.strat_5} approach={state.approach} goals_1={state.goals_1} goals_2={state.goals_2} goals_3={state.goals_3} goals_4={state.goals_4} results_1={state.results_1} results_2={state.results_2} results_3={state.results_3} results_4={state.results_4} quote={state.quote} name={state.name} relation={state.relation} />
        </section>
    );
}

export default ProjectSection;
