
// import React, { Fragment } from 'react';
// import Link from 'next/link'
// import { Dialog, Grid, } from '@mui/material'
// import blog3 from '/public/images/blog-details/comments-author/img-1.jpg'
// import blog4 from '/public/images/blog-details/comments-author/img-2.jpg'
// import blog5 from '/public/images/blog-details/comments-author/img-3.jpg'
// import blog6 from '/public/images/blog-details/author.jpg'
// import gl1 from '/public/images/blog-details/1.jpg'
// import gl2 from '/public/images/blog-details/2.jpg'
// import Image from 'next/image';

// const submitHandler = (e) => {
//     e.preventDefault()
// }


// const BlogSingle = ({ maxWidth, open, onClose, title, bImg, create_at, author }) => {

//     return (
//         <Fragment>
//             <Dialog
//                 open={open}
//                 onClose={onClose}
//                 className="modalWrapper quickview-dialog"
//                 maxWidth={maxWidth}
//             >
//                 <Grid className="modalBody modal-body">
//                     <button className='modal-close' onClick={onClose}><i className='fa fa-close'></i></button>
//                     <section className="tp-blog-single-section">
//                         <div className="container">
//                             <div className="row">
//                                 <div className='col col-lg-12 col-12'>
//                                     <div className="tp-blog-content">
//                                         <div className="post format-standard-image">
//                                             <div className="entry-media">
//                                                 <Image src={bImg} alt="" />
//                                             </div>
//                                             <div className="entry-meta">
//                                                 <ul>
//                                                     <li><i className="fi flaticon-user"></i> By <Link href="/">{author}</Link> </li>
//                                                     {/* <li><i className="fi flaticon-comment-white-oval-bubble"></i> Comments {comment}</li> */}
//                                                     <li><i className="fi flaticon-calendar"></i> {create_at}</li>
//                                                 </ul>
//                                             </div>
//                                             <h2>{title}</h2>
//                                             <p>I'm excited to share that I've just completed my 19-week program at Hack Reactor.

// Looking back at my time with Hack Reactor, I truly owe a debt of gratitude to the Instructors, cohort leads, seirs, and career service members for their unwavering support and guidance throughout my learning journey. Without their dedication and commitment, I would not have been able to achieve my goals and advance my skills in the way that I have.

// The collaborative nature of Hack Reactor made it an incredibly supportive and inclusive environment, and I feel fortunate to have been a part of it. Working alongside such talented individuals not only helped me grow as a programmer but also gave me the opportunity to build meaningful connections with like-minded people in the industry. Crazy enough, one of our amazingly talented classmates, Mischa Goodman, is coming to my hometown. She will present her film at an international film festival in precisely one month. Without Hack Reactor I would have never known about this, but now I can't wait to support her and her amazing work!

// I can't wait to see what comes next, and thank you to everyone I've interacted with these past 19 weeks, you're all amazing in your own ways!
// </p>
//                                             <blockquote>
//                                             "You don't have to be great to start, but you have to start to be great" (Ziglar)
//                                             </blockquote>
//                                             <p>I am excited to announce that I have been in contact with a local Pita Pit and will be working on a small project for them. I can't wait to get started and will be sure to share updates as soon as they become available. Stay tuned for more details on this exciting project! </p>

//                                             <div className="gallery">
//                                                 <div>
//                                                     <Image src={gl1} alt="" />
//                                                 </div>
//                                                 <div>
//                                                     <Image src={gl2} alt="" />
//                                                 </div>
//                                             </div>
//                                         </div>

//                                         <div className="tag-share clearfix">
//                                             <div className="tag">
//                                                 <span>Share: </span>
//                                                 <ul>
//                                                 <li><Link href="https://www.linkedin.com/feed/update/urn:li:activity:7044420631634870272/">linkedin</Link></li>
//                                                 <li><Link href="https://gitlab.com/ZacharyD">Gitlab</Link></li>
//                                                     {/* <li><Link href="/">Creative</Link></li> */}
//                                                 </ul>
//                                             </div>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                     </section>
//                 </Grid>
//             </Dialog>
//         </Fragment>
//     );
// }
// export default BlogSingle


import blogs from '../../api/blogs'
import React, { Fragment } from 'react';
import Link from 'next/link'
import { Dialog, Grid, } from '@mui/material'
import blog3 from '/public/images/blog-details/comments-author/img-1.jpg'
import blog4 from '/public/images/blog-details/comments-author/img-2.jpg'
import blog5 from '/public/images/blog-details/comments-author/img-3.jpg'
import blog6 from '/public/images/blog-details/author.jpg'
import gl1 from '/public/images/blog-details/1.jpg'
import gl2 from '/public/images/blog-details/2.jpg'
import Image from 'next/image';

const submitHandler = (e) => {
    e.preventDefault()
}

const BlogSingle = ({ maxWidth, open, onClose, title, bImg, create_at, author, top_body, quote, bottom_body }) => {

    return (
        <Fragment>
            <Dialog
                open={open}
                onClose={onClose}
                className="modalWrapper quickview-dialog"
                maxWidth={maxWidth}>
                <Grid className="modalBody modal-body">
                    <button className='modal-close' onClick={onClose}><i className='fa fa-close'></i></button>
                    <section className="tp-blog-single-section">
                        <div className="container">
                            <div className="row">
                                <div className='col col-lg-12 col-12'>
                                    <div className="tp-blog-content">
                                        <div className="post format-standard-image">
                                            <div className="entry-media">
                                                <Image src={bImg} alt="" />
                                            </div>
                                            <div className="entry-meta">
                                                <ul>
                                                    <li><i className="fi flaticon-user"></i> By <Link href="/">{author}</Link> </li>
                                                    <li><i className="fi flaticon-calendar"></i> {create_at}</li>
                                                </ul>
                                            </div>
                                            <h2>{title}</h2>
                                            <p>{top_body}</p>
                                            <blockquote>
                                            {quote}
                                            </blockquote>
                                            <p>{bottom_body}</p>
                                            <div className="gallery">
                                                <div>
                                                    <Image src={gl1} alt="" />
                                                </div>
                                                <div>
                                                    <Image src={gl2} alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="tag-share clearfix">
                                            <div className="tag">
                                                <span>Share: </span>
                                                <ul>
                                                <li><Link href="https://www.linkedin.com/feed/update/urn:li:activity:7044420631634870272/">linkedin</Link></li>
                                                <li><Link href="https://gitlab.com/ZacharyD">Gitlab</Link></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </Grid>
            </Dialog>
        </Fragment>
    );
}

export default BlogSingle;
