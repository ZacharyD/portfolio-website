import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Testimonials = [
    {
        name: 'A.J. Unsen',
        title: 'Business Owner/operator',
        description: '“Exactly what we needed for our business! I cant thank him enough and am very appreciative of everything he has done for my business.”',
    },
    {
        name: 'Kyle Tran',
        title: 'Software Engineer',
        description: '“Zach is an exceptional team player and a true force to be reckoned with. During our Bootcamp, we collaborated on a full-stack project, and I was thoroughly impressed by his perseverance and technical skills.”',
    },
    {
        name: 'Rowan Minor',
        title: 'Software Engineer/Photographer',
        description: '“Zach demonstrated exceptional skills in collaborating with the team to conceptualize and plan the project, as well as contributing his expertise in”',
    },
    {
        name: 'Mischa Goodman',
        title: 'Software Engineer',
        description: '“Zach is an awesome person to have on a team. He works hard, never gives up and brings exciting ideas to the table. I was impressed with both his technical and soft skills.”',
    },
]



const Testimonial = () => {

    var settings = {
        dots: false,
        arrows: false,
        speed: 4000,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        responsive: [
            {
                breakpoint: 1500,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (

        <section className="tp-testimonial-section section-padding">
            <div className="container">
                <div className="tp-section-title">
                    <span>Testimonials</span>
                    <h2>What My clients say</h2>
                </div>

                <div className="tp-testimonial-wrap">
                    <Slider {...settings}>
                        {Testimonials.map((tstml, tsm) => (
                            <div className="tp-testimonial-item" key={tsm}>
                                <div className="tp-testimonial-text">
                                    <p>{tstml.description}</p>
                                    <span>{tstml.name}</span>
                                </div>
                            </div>
                        ))}
                    </Slider>
                </div>
            </div>
        </section>
    )
}

export default Testimonial;
