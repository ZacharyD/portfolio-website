// images
import blogImg1 from "/public/images/blog/grad.jpg";
import blogImg2 from "/public/images/blog/img-2.jpg";
import blogImg3 from "/public/images/blog/mainPita.jpg";
import blogImg4 from "/public/images/blog/img-4.jpg";
import blogImg5 from "/public/images/blog/img-5.jpg";
import blogImg6 from "/public/images/blog/img-6.jpg";

import blogSingleImg1 from "/public/images/blog-details/img-1.jpg";
import blogSingleImg2 from "/public/images/blog-details/img-2.jpg";
import blogSingleImg3 from "/public/images/blog-details/img-3.jpg";
import blogSingleImg4 from "/public/images/blog-details/img-4.jpg";
import blogSingleImg5 from "/public/images/blog-details/img-5.jpg";
import blogSingleImg6 from "/public/images/blog-details/img-6.jpg";


const blogs = [
    {
    id: '1',
        title: 'Pita Pit Dubuque',
        screens: blogImg3,
        description: 'Excited to announce a new website I built for a local business',
        author: 'Zach Dempsey',
        thumb:'Developer',
        create_at: '26 March, 2023',
        blogSingleImg:blogSingleImg3,
        // comment:'95',
        // blClass:'format-video',
        top_body: "I recently built a website for a local business that needed a way for job applicants or anyone with potential business offers to contact them easily. I first met the owner of the business while working for FedEx Express. The workers were always so nice and one in particular remembered my order every time I came in regardless of how long it was since my last visit. The owner Aj knew I was a new software engineer looking for work and when he mentioned his idea, I didn't hesitate to say I'd get it done. Not going to lie, my main motivation were the free sandwiches I'd receive in return. If you're ever in Dubuque, Iowa or you're already here, stop in at 2515 NW Arterial, Suite 7. The food is amazing and the staff are the absolute best.",
        quote: '"Are you getting the usual? You get the chicken fajita, right?" ()',
        bottom_body: "My biggest takeaway from completing this project would be simply caring for others. You never know what a kind word or cheerful attitude can do for someone's day. It may even help you down the road and lead to new opportunities for yourself or your business.",
    },
    {
        id: '2',
        title: 'Graduating Hack Reactor and what comes next',
        screens: blogImg1,
        description: 'LinkedIn Announcement',
        author: 'Zach Dempsey',
        thumb:'Software Engineer',
        create_at: '24 March, 2023',
        // blogSingleImg:blogSingleImg1,
        // comment:'35',
        // blClass:'format-standard-image',
        top_body: "I'm excited to share that I've just completed my 19-week program at Hack Reactor. Looking back at my time with Hack Reactor, I truly owe a debt of gratitude to the Instructors, cohort leads, seirs, and career service members for their unwavering support and guidance throughout my learning journey. Without their dedication and commitment, I would not have been able to achieve my goals and advance my skills in the way that I have. The collaborative nature of Hack Reactor made it an incredibly supportive and inclusive environment, and I feel fortunate to have been apart of it. Working alongside such talented individuals not only helped me grow as a programmer but also gave me the opportunity to build meaningful connections with like-minded people in the industry. Crazy enough, one of our amazingly talented classmates, Mischa Goodman, is coming to my hometown. She will present her film at an international film festival in precisely one month. Without Hack Reactor I would have never known about this, but now I can't wait to support her and her amazing work! I can't wait to see what comes next, and thank you to everyone I've interacted with these past 19 weeks, you're all amazing in your own  ways!",
        quote: '"You dont have to be great to start, but you have to start to be great" (Ziglar)',
        bottom_body: "I am excited to announce that I have been in contact with a local Pita Pit and will be working on a small project for them. I can't wait to get started and will be sure to share updates as soon as they become available. Stay tuned for more details on this exciting project!",

    },
    {
        id: '3',
        title: 'One App, Endless Possibilities Explore the World with Tripager',
        screens: blogImg2,
        description: 'Plan Your Dream Vacation with Ease Introducing Tripager.',
        author: 'Z Dempsey, Mischa Goodman, Kyle Tran, and Andrew Fockler',
        thumb:'Developer',
        create_at: '16 March,2023',
        // blogSingleImg:blogSingleImg2,
        // comment:'80',
        // blClass:'format-standard-image',
        top_body: "I just completed my final project for my boot camp at Hack Reactor. I'm so proud of how far my team and myself have come since the beginning of mod 3. It was truly a pleasure to work with everyone and I can't wait to see what we can make next!",
        quote: '"I want to make something I can be proud of" (Mischa Goodman)',
        bottom_body: "",


    },
    {
        id: '4',
        title: 'Automobile Service Industry Deployment in progress',
        screens: blogImg3,
        description: "I'm thrilled to announce the launch of a new website that I helped create.",
        author: 'Z Dempsey, Rowan Minor',
        thumb:'Developer',
        create_at: '28 January,2023',
        blogSingleImg:blogSingleImg3,
        // comment:'95',
        // blClass:'format-video',
        top_body: "The one stop shop service for all of your dealership needs. I just completed my first website and group project. ",
        quote: '',
        bottom_body: "",
    },
    // {
    //     id: '5',
    //     title: 'Many desktop publish package editors use.',
    //     screens: blogImg4,
    //     description: 'Consectetur adipiscing elit. Purusout phasellus malesuada lectus.',
    //     author: 'Biry',
    //     thumb:'Fasion',
    //     create_at: '13 Dec,2023',
    //     blogSingleImg:blogSingleImg4,
    //     comment:'80',
    // //     blClass:'format-standard-image',
        // top_body: "this is my description of the project",
        // quote: '"I want to make something I can be proud of" (Mischa Goodman)',
        // bottom_body: "how about another part maybe after a quote",
    // },
    // {
    //     id: '5',
    //     title: 'The definitive list of digital products you can sell',
    //     screens: blogImg5,
    //     description: 'Consectetur adipiscing elit. Purusout phasellus malesuada lectus.',
    //     author: 'Sharah',
    //     thumb:'Developer',
    //     create_at: '13 Dec,2023',
    //     blogSingleImg:blogSingleImg5,
    //     comment:'80',
    //     blClass:'format-standard-image',
    // },
    // {
    //     id: '6',
    //     title: 'Nemo enim ipsam voluptatem quia voluptas.',
    //     screens: blogImg6,
    //     description: 'Consectetur adipiscing elit. Purusout phasellus malesuada lectus.',
    //     author: 'Maria',
    //     thumb:'Fasion',
    //     create_at: '22 Dec,2023',
    //     blogSingleImg:blogSingleImg6,
    //     comment:'95',
    //     blClass:'format-video',
    // },
];
export default blogs;
