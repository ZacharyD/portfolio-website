import Image from 'next/image';
import React from 'react'
import aImg from '/public/images/about/best.jpg'

const About = (props) => {
    return (

        <section className="tf-about-section section-padding">
            <div className="container">
                <div className="tf-about-wrap">
                    <div className="row align-items-center">
                        <div className="col-lg-6 col-md-12 col-12">
                            <div className="tf-about-img">
                                <Image src={aImg} alt="" />
                                {/* <div className="tf-about-img-text">
                                    <div className="tf-about-icon">
                                        <h3>8+</h3>
                                        <span>Years Exprience</span>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-12 col-12">
                            <div className="tf-about-text">
                                <small>about Me</small>
                                <h2>Welcome to Zach Dempsey's Portfolio</h2>
                                <p>I entered the tech industry with a strong desire to be a part of something that makes a positive difference in people's lives.

I used to think I would enjoy a career at FedEx, being able to be on my own, enjoy the different scenery, and see the animals day in and day out. Now I want to be able to save time commuting every day, to have that extra time to cook dinner, spend time with loved ones, or just take a little time to myself because driving through snow in the Midwest is a nightmare most days.

I’m most excited to work on front-end development regardless of where my career takes me. Working with new frameworks and libraries to create interactive web pages has been fascinating to me these past months. I look forward to interacting with a team that I could learn from and grow as a software engineer. Looking back at my time with Hack Reactor, I realize that without those experiences I made along the way, I wouldn’t be where I am today. I truly owe a debt of gratitude to the Instructors, cohort leads, seirs, and career service members for their unwavering support and guidance throughout my learning journey. Without their dedication and commitment, I would not have been able to achieve my goals and advance my skills in the way that I have.

I have a passion for front-end web design, which at the start of my journey I wanted nothing to do with. My familiarity with React, Bootstrap, JavaScript, and my ability to collaborate with others, makes me a promising candidate, who understands the field is constantly evolving and our learning is never truly finished. I bring my enthusiasm and dedication to any team environment I work in. With my passion for technology and desire to help others, I am confident I will make a positive impact in the tech industry, helping create innovative and user-friendly web experiences that will benefit everyone.

As someone who relied on these applications on a daily basis, My experience as a driver has taught me the importance of software functionality and I am committed to creating software that is user-friendly and accessible to all. I know the importance of a well-functioning application that also provides a user perspective other developers may not have. This is why back-end web development is also important, with my knowledge of Python, PostgreSQL, and API integration, I am capable of creating scalable and efficient back-end systems that can support complex web applications. I understand the importance of creating a reliable and secure back-end infrastructure that can handle high volumes of data and traffic.

I also currently work at an Assisted Living apartment where I do maintenance for the building.
Working with people less fortunate than most has made me more understanding of others' needs and humbled me vastly. I love being able to help others and would enjoy a career that allows me to give back.

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default About;
